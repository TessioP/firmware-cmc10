/*
 * Encoder.c
 *
 *  Created on: Sep 19, 2018
 *      Author: mmaximo
 */

#include "Encoder.h"
#include "definitions.h"

void initializeEncoder(Encoder* encoder, TIM_HandleTypeDef* encoderTimer,
		float samplingFrequency) {
	encoder->encoderCounter = encoderTimer;
	encoder->samplingFrequency = samplingFrequency;
	encoder->pulsesToRpm = 60.0 * samplingFrequency / (PULSES_PER_TURN);
	encoder->pulsesToRad = encoder->pulsesToRpm * RPM_TO_SI;
	HAL_TIM_Encoder_Start(encoderTimer, TIM_CHANNEL_ALL);
	encoder->currentEncoderCount = 0;
	encoder->previousEncoderCount = 0;
}

void updateEncoder(Encoder* encoder) {
	encoder->previousEncoderCount = encoder->currentEncoderCount;
	encoder->currentEncoderCount = __HAL_TIM_GET_COUNTER(
			encoder->encoderCounter);
}

int16_t getEncoderDiff(Encoder* encoder) {
	return encoder->currentEncoderCount - encoder->previousEncoderCount;
}

float getCurrentEncoderSpeed(Encoder* encoder) {
	return getEncoderDiff(encoder) * encoder->pulsesToRad;
}

float getCurrentEncoderSpeedRPM(Encoder* encoder) {
	return getEncoderDiff(encoder) * encoder->pulsesToRpm;
}
