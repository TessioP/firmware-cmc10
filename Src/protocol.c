/*
 * protocol.c
 *
 *  Created on: Aug 8, 2018
 *      Author: tessi
 */

#include "protocol.h"

void dataHandlerInit(dataHandler* dataHandler, uint8_t robotID) {
	// Checksum
	dataHandler->checksumOK = 0;

	// Variables relating the PIDs
	dataHandler->Kp = 0.0;
	dataHandler->Ki = 0.0;
	dataHandler->Kd = 0.0;
	dataHandler->hasToUpdateGainsPID = 0;

	// Must report battery voltage?
	dataHandler->hasToReadBatteryVoltage = 0;

	// Must report motors velocity?
	dataHandler->hasToReadMotorSpeed = 0;

	// Set reference speed?
	dataHandler->hasToWriteMotorSpeed = 0;
	dataHandler->leftMotorSpeed = 0.0;
	dataHandler->rightMotorSpeed = 0.0;

	//Robot ID
	dataHandler->robotID = robotID;

	// Need to send an answer to master?
	dataHandler->needToRespond = 0;
}

float hex2float(char *packet) {
	float decodedValue;
	memcpy(&decodedValue, packet, 4);

	return decodedValue;
}

void float2hex(float number, char *data) {
	memcpy(data, &number, 4);
}

int16_t hex2int16(char *packet) {
	int16_t decodedValue;
	memcpy(&decodedValue, packet, 2);

	return decodedValue;
}

int verifyCkecksum(char *packet) {
	int ok = 0;
	uint8_t count = 0, i;

	for (i = 0; i < (MSG_SIZE - 1); i++)
		count = count + packet[i];

	if (count == packet[MSG_SIZE - 1])
		ok = 1;

	return ok;
}

void decode(dataHandler* dataHandler, char* packet) {
	char whatToDo = packet[0];
	uint8_t from = 1;

	dataHandler->checksumOK = verifyCkecksum(packet);

	if (dataHandler->checksumOK) {
		// Write left PID?
		if (whatToDo & (1 << WPID)) {
			dataHandler->hasToUpdateGainsPID = 1;
			dataHandler->Kp = hex2float(&packet[from]);
			from = from + 4;
			dataHandler->Ki = hex2float(&packet[from]);
			from = from + 4;
			dataHandler->Kd = hex2float(&packet[from]);
			from = from + 4;
		} else {
			// So, do not need to update PID gains
			dataHandler->hasToUpdateGainsPID = 0;

			// Write right motor speed?
			if (whatToDo & (1 << WMS)) {
				// Select the right message chunk
				// See documentation on
				// https://docs.google.com/document/d/1L3LqoGH7eoP1DZyCqrG9xFrwT__L3e1usR-vqD7ht1Y/edit?usp=sharing
				from = from + 4 * (dataHandler->robotID - 1);

				// Extract values
				dataHandler->hasToWriteMotorSpeed = 1;
				dataHandler->leftMotorSpeed = hex2int16(&packet[from]);
				from = from + 2;
				dataHandler->rightMotorSpeed = hex2int16(&packet[from]);
			} else {
				dataHandler->hasToWriteMotorSpeed = 0;
			}
		}
	} else {
		dataHandler->hasToUpdateGainsPID = 0;
		dataHandler->hasToWriteMotorSpeed = 0;
		dataHandler->hasToReadMotorSpeed = 0;
		dataHandler->hasToReadBatteryVoltage = 0;
		dataHandler->needToRespond = 0;
	}
}

void getMotorSpeed(dataHandler * dataHandler, int16_t *leftMotorVelocity,
		int16_t *rightMotorVelocity) {
	*leftMotorVelocity = dataHandler->leftMotorSpeed;
	*rightMotorVelocity = dataHandler->rightMotorSpeed;
	dataHandler->hasToWriteMotorSpeed = 0;
}
