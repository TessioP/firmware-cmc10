/*
 * Motor.c
 *
 *  Created on: Sep 19, 2018
 *      Author: mmaximo
 */

#include "Motor.h"

void initializeMotor(Motor * motor, TIM_HandleTypeDef *pwmTimer, int channelA,
		int channelB, TIM_HandleTypeDef *encoderTimer, int type,
		float samplingFrequency) {

	//Selecting pwm channels
	motor->pwmTimer = pwmTimer;
	motor->channelA = channelA;
	motor->channelB = channelB;
	motor->type = type;
	motor->maxPwm = pwmTimer->Init.Period;
	//Starting PWM
	HAL_TIM_PWM_Start(motor->pwmTimer, motor->channelA);
	HAL_TIM_PWM_Start(motor->pwmTimer, motor->channelB);

	//Starting sensor
	initializeEncoder(&(motor->encoder), encoderTimer, samplingFrequency);

}

void setDriverCommand(Motor* motor, int pwm) {
	/**
	 * It is worth noting that if the hardware registers are
	 * of type uint32_t. For this reason, the pwm variable must
	 * be always positive. Therefore, if pwm is negative, then
	 * it will pass -pwm to the hardware
	 */
	if (pwm > motor->maxPwm)
		pwm = motor->maxPwm;
	if (pwm < -motor->maxPwm)
		pwm = -motor->maxPwm;

	if (motor->type == MOTOR_LEFT) {
		if (pwm > 0) {
			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelA, pwm);
			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelB, 0);
		} else {
			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelA, 0);
			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelB, -pwm);
		}
	} else if (motor->type == MOTOR_RIGHT) {
		if (pwm > 0) {
			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelB, pwm);
			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelA, 0);
		} else {
			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelB, 0);
			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelA, -pwm);
		}
	}
//	if (motor->type == MOTOR_LEFT) {
//		if (pwm > 0) {
//			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelA, motor->maxPwm - pwm);
//			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelB, motor->maxPwm);
//		} else {
//			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelA, motor->maxPwm);
//			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelB, motor->maxPwm + pwm);
//		}
//	} else if (motor->type == MOTOR_RIGHT) {
//		if (pwm > 0) {
//			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelB, motor->maxPwm - pwm);
//			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelA, motor->maxPwm);
//		} else {
//			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelB, motor->maxPwm);
//			__HAL_TIM_SET_COMPARE(motor->pwmTimer, motor->channelA, motor->maxPwm + pwm);
//		}
//	}
}

float getCurrentMotorSpeed(Motor* motor) {
	float speed = getCurrentEncoderSpeed(&(motor->encoder));

	if (motor->type == MOTOR_RIGHT)
		speed = -speed;

	return speed;
}

void updateMotor(Motor* motor) {
	updateEncoder(&(motor->encoder));
}
