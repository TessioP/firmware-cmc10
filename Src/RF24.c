/*
 * RF24.c
 *
 *  Created on: 31 de mai de 2018
 *      Author: washi
 */


/*
 Copyright (C) 2011 J. Coliz <maniacbug@ymail.com>

 This program is free software; you can redistribute it and/or
 modify it under the terms of the GNU General Public License
 version 2 as published by the Free Software Foundation.
 */

#include "RF24.h"

#include "nRF24L01.h"
#include "RF24_config.h"
#include "main.h"

#define true  1
#define false 0

inline void delay1Us() {
    int i;
    for (i = 0; i < 4; i += 1);
}

inline void _delay_us(uint16_t us) {
    int i;
    for (i = 0; i < us; i++)
        delay1Us();
}

uint8_t SPI_MASTER_TRANSCEIVER(RF24 * rf24, uint8_t x) {
    uint8_t tx_data[1] = {x}, rx_data[1];
    HAL_SPI_TransmitReceive(rf24->_spi, tx_data, rx_data, 1, 1000);
    return rx_data[0];
}


/****************************************************************************/

void csn(int mode) {
    if (mode == 1)
        HAL_GPIO_WritePin(CSN_RADIO_GPIO_Port, CSN_RADIO_Pin, GPIO_PIN_SET);
    else
        HAL_GPIO_WritePin(CSN_RADIO_GPIO_Port, CSN_RADIO_Pin, GPIO_PIN_RESET);

}

/****************************************************************************/

void ce(int level) {
    if (level == 1)
        HAL_GPIO_WritePin(CE_GPIO_Port, CE_Pin, GPIO_PIN_SET);
    else
        HAL_GPIO_WritePin(CE_GPIO_Port, CE_Pin, GPIO_PIN_RESET);
}

/****************************************************************************/

void beginTransaction() {
    csn(0);
}

/****************************************************************************/

void endTransaction() {
    csn(1);
}

/****************************************************************************/

uint8_t read_register(RF24 * rf24, uint8_t reg, uint8_t *buf, uint8_t len) {
    uint8_t status;

    beginTransaction();
    status = SPI_MASTER_TRANSCEIVER(rf24, R_REGISTER | (REGISTER_MASK & reg));
    while (len--) {
        *buf++ = SPI_MASTER_TRANSCEIVER(rf24, 0xff);
    }
    endTransaction();

    return status;
}

/****************************************************************************/

uint8_t read_register_byte(RF24 * rf24, uint8_t reg) {
    uint8_t result;

    beginTransaction();
    SPI_MASTER_TRANSCEIVER(rf24, R_REGISTER | (REGISTER_MASK & reg));
    result = SPI_MASTER_TRANSCEIVER(rf24, 0xff);
    endTransaction();

    return result;
}

/****************************************************************************/

uint8_t write_register(RF24 * rf24, uint8_t reg, uint8_t *buf, uint8_t len) {
    uint8_t status;

    beginTransaction();
    status = SPI_MASTER_TRANSCEIVER(rf24, W_REGISTER | (REGISTER_MASK & reg));
    while (len--)
        SPI_MASTER_TRANSCEIVER(rf24, *buf++);
    endTransaction();

    return status;
}

/****************************************************************************/

uint8_t write_register_byte(RF24 * rf24, uint8_t reg, uint8_t value) {
    uint8_t status;

    beginTransaction();
    status = SPI_MASTER_TRANSCEIVER(rf24, W_REGISTER | (REGISTER_MASK & reg));
    SPI_MASTER_TRANSCEIVER(rf24, value);
    endTransaction();

    return status;
}

/****************************************************************************/

uint8_t write_payload(RF24 * rf24, void *buf, uint8_t data_len, uint8_t writeType) {
    uint8_t status;
    uint8_t *current = (uint8_t *)(buf);

    data_len = rf24_min(data_len, rf24->payload_size);
    uint8_t blank_len = rf24->dynamic_payloads_enabled ? 0 : rf24->payload_size - data_len;

    beginTransaction();
    status = SPI_MASTER_TRANSCEIVER(rf24, writeType);
    while (data_len--) {
        SPI_MASTER_TRANSCEIVER(rf24, *current++);
    }
    while (blank_len--) {
        SPI_MASTER_TRANSCEIVER(rf24, 0);
    }
    endTransaction();

    return status;
}

/****************************************************************************/

uint8_t read_payload(RF24 * rf24, void *buf, uint8_t data_len) {
    uint8_t status;
    uint8_t *current = (uint8_t *)(buf);

    if (data_len > rf24->payload_size) data_len = rf24->payload_size;
    uint8_t blank_len = rf24->dynamic_payloads_enabled ? 0 : rf24->payload_size - data_len;

    beginTransaction();
    status = SPI_MASTER_TRANSCEIVER(rf24, R_RX_PAYLOAD);
    while (data_len--) {
        *current++ = SPI_MASTER_TRANSCEIVER(rf24, 0xFF);
    }
    while (blank_len--) {
        SPI_MASTER_TRANSCEIVER(rf24, 0xff);
    }
    endTransaction();

    return status;
}

/****************************************************************************/

uint8_t flush_rx(RF24 * rf24) {
    return spiTrans(rf24, FLUSH_RX);
}

/****************************************************************************/

uint8_t flush_tx(RF24 * rf24) {
    return spiTrans(rf24, FLUSH_TX);
}

/****************************************************************************/

uint8_t spiTrans(RF24 * rf24, uint8_t cmd) {

    uint8_t status;

    beginTransaction();
    status = SPI_MASTER_TRANSCEIVER(rf24, cmd);
    endTransaction();

    return status;
}

/****************************************************************************/

uint8_t get_status(RF24 * rf24) {
    return spiTrans(rf24, NOP);
}

/****************************************************************************/

void initializeRF24(RF24 * rf24, GPIO_TypeDef *CEport, uint16_t CEpin, GPIO_TypeDef *CSNport, uint16_t CSNpin, SPI_HandleTypeDef *spi)
{
    rf24->_CEport = CEport;
    rf24->_CSNport = CSNport;
    rf24->_spi = spi;
    rf24->p_variant = false;
    rf24->payload_size = 32;
    rf24->dynamic_payloads_enabled = false;
    rf24->addr_width = 5;
	rf24->csDelay = 5; //,pipe0_reading_address(0)

    rf24->pipe0_reading_address[0] = 0;
    rf24->_CEpin = CEpin;
    rf24->_CSNpin = CSNpin;
    rf24->txDelay = 0;
}

/****************************************************************************/

void setChannel(RF24 * rf24, uint8_t channel) {
    uint8_t max_channel = 125;
    write_register_byte(rf24, RF_CH, rf24_min(channel, max_channel));
}

uint8_t getChannel(RF24 * rf24) {

    return read_register_byte(rf24, RF_CH);
}

/****************************************************************************/

void setPayloadSize(RF24 * rf24, uint8_t size) {
    rf24->payload_size = rf24_min(size, 32);
}

/****************************************************************************/

uint8_t getPayloadSize(RF24 * rf24) {
    return rf24->payload_size;
}

int begin(RF24 * rf24) {

    uint8_t setup = 0;

    ce(0);
    csn(1);
    delay(100);

    // Must allow the radio time to settle else configuration bits will not necessarily stick.
    // This is actually only required following power up but some settling time also appears to
    // be required after resets too. For full coverage, we'll always assume the worst.
    // Enabling 16b CRC is by far the most obvious case if the wrong timing is used - or skipped.
    // Technically we require 4.5ms + 14us as a worst case. We'll just call it 5ms for good measure.
    // WARNING: Delay is based on P-variant whereby non-P *may* require different timing.
    delay(5);

    // Reset NRF_CONFIG and enable 16-bit CRC.
    write_register_byte(rf24, NRF_CONFIG, 0x0C);

    // Set 1500uS (minimum for 32B payload in ESB@250KBPS) timeouts, to make testing a little easier
    // WARNING: If this is ever lowered, either 250KBS mode with AA is broken or maximum packet
    // sizes must never be used. See documentation for a more complete explanation.
    setRetries(rf24, 5, 15);

    // Reset value is MAX
    //setPALevel( RF24_PA_MAX ) ;

    // check for connected module and if this is a p nRF24l01 variant
    //
    if (setDataRate(rf24, RF24_250KBPS)) {
        rf24->p_variant = true;
    }
    setup = read_register_byte(rf24, RF_SETUP);
    /*if( setup == 0b00001110 )     // register default for nRF24L01P
    {
      p_variant = true ;
    }*/

    // Then set the data rate to the slowest (and most reliable) speed supported by all
    // hardware.
    setDataRate(rf24, RF24_1MBPS);

    // Initialize CRC and request 2-byte (16bit) CRC
    //setCRCLength( RF24_CRC_16 ) ;

    // Disable dynamic payloads, to match dynamic_payloads_enabled setting - Reset value is 0
    toggle_features(rf24);
    write_register_byte(rf24, FEATURE, 0);
    write_register_byte(rf24, DYNPD, 0);
    rf24->dynamic_payloads_enabled = false;

    // Reset current status
    // Notice reset and flush is the last thing we do
    write_register_byte(rf24, NRF_STATUS, _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT));

    // Set up default configuration.  Callers can always change it later.
    // This channel should be universally safe and not bleed over into adjacent
    // spectrum.
    setChannel(rf24, 76);

    // Flush buffers
    flush_rx(rf24);
    flush_tx(rf24);

    powerUp(rf24); //Power up by default when begin() is called

    // Enable PTX, do not write CE high so radio will remain in standby I mode ( 130us max to transition to RX or TX instead of 1500us from powerUp )
    // PTX should use only 22uA of power
    write_register_byte(rf24, NRF_CONFIG, (read_register_byte(rf24, NRF_CONFIG)) & ~_BV(PRIM_RX));

    // if setup is 0 or ff then there was no response from module
    return (setup != 0 && setup != 0xff);
}

/****************************************************************************/

int isChipConnected(RF24 * rf24) {
    uint8_t setup = read_register_byte(rf24, SETUP_AW);
    if (setup >= 1 && setup <= 3) {
        return true;
    }

    return false;
}

/****************************************************************************/

void startListening(RF24 * rf24) {
#if !defined (RF24_TINY) && !defined(LITTLEWIRE)
    powerUp(rf24);
#endif
    write_register_byte(rf24, NRF_CONFIG, read_register_byte(rf24, NRF_CONFIG) | _BV(PRIM_RX));
    write_register_byte(rf24, NRF_STATUS, _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT));
    ce(1);
    // Restore the pipe0 adddress, if exists
    if (rf24->pipe0_reading_address[0] > 0) {
        write_register(rf24, RX_ADDR_P0, rf24->pipe0_reading_address, rf24->addr_width);
    } else {
        closeReadingPipe(rf24, 0);
    }

    // Flush buffers
    //flush_rx();
    if (read_register_byte(rf24, FEATURE) & _BV(EN_ACK_PAY)) {
        flush_tx(rf24);
    }

    // Go!
    //delayMicroseconds(100);
}

/****************************************************************************/
static uint8_t child_pipe_enable[] PROGMEM =
        {
                ERX_P0, ERX_P1, ERX_P2, ERX_P3, ERX_P4, ERX_P5
        };

void stopListening(RF24 * rf24) {
    ce(0);

    delayMicroseconds(150);

    if (read_register_byte(rf24, FEATURE) & _BV(EN_ACK_PAY)) {
        delayMicroseconds(150); //200
        flush_tx(rf24);
    }
    //flush_rx();
    write_register_byte(rf24, NRF_CONFIG, (read_register_byte(rf24, NRF_CONFIG)) & ~_BV(PRIM_RX));

#if defined (RF24_TINY) || defined (LITTLEWIRE)
    // for 3 pins solution TX mode is only left with additonal powerDown/powerUp cycle
    if (ce_pin == csn_pin) {
      powerDown(rf24);
      powerUp(rf24);
    }
#endif
    write_register_byte(rf24, EN_RXADDR,
                   read_register_byte(rf24, EN_RXADDR) | _BV(pgm_read_byte(&child_pipe_enable[0]))); // Enable RX on pipe0

    //delayMicroseconds(100);

}

/****************************************************************************/

void powerDown(RF24 * rf24) {
    ce(0); // Guarantee CE is low on powerDown
    write_register_byte(rf24, NRF_CONFIG, read_register_byte(rf24, NRF_CONFIG) & ~_BV(PWR_UP));
}

/****************************************************************************/

//Power up now. Radio will not power down unless instructed by MCU for config changes etc.
void powerUp(RF24 * rf24) {
    uint8_t cfg = read_register_byte(rf24, NRF_CONFIG);

    // if not powered up then power up and wait for the radio to initialize
    if (!(cfg & _BV(PWR_UP))) {
        write_register_byte(rf24, NRF_CONFIG, cfg | _BV(PWR_UP));

        // For nRF24L01+ to go from power down mode to TX or RX mode it must first pass through stand-by mode.
        // There must be a delay of Tpd2stby (see Table 16.) after the nRF24L01+ leaves power down mode before
        // the CEis set high. - Tpd2stby can be up to 5ms per the 1.0 datasheet
        delay(5);
    }
}

/******************************************************************/
#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
void errNotify(){
#if defined (SERIAL_DEBUG) || defined (RF24_LINUX)
      printf_P(PSTR("RF24 HARDWARE FAIL: Radio not responding, verify pin connections, wiring, etc.\r\n"));
#endif
#if defined (FAILURE_HANDLING)
    failureDetected = 1;
#else
    delay(5000);
#endif
}
#endif
/******************************************************************/

//Similar to the previous write, clears the interrupt flags
int writeMulticast(RF24 * rf24, void *buf, uint8_t len, int multicast) {
    //Start Writing
    startFastWrite(rf24, buf, len, multicast, 1);

    //Wait until complete or failed
#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
    uint32_t timer = millis();
#endif

    while (!(get_status(rf24) & (_BV(TX_DS) | _BV(MAX_RT)))) {
#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
        if(millis() - timer > 95){
            errNotify();
#if defined (FAILURE_HANDLING)
              return 0;
#else
              delay(100);
#endif
        }
#endif
    }

    ce(0);

    uint8_t status = write_register_byte(rf24, NRF_STATUS, _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT));

    //Max retries exceeded
    if (status & _BV(MAX_RT)) {
        flush_tx(rf24); //Only going to be 1 packet int the FIFO at a time using this method, so just flush
        return 0;
    }
    //TX OK 1 or 0
    return 1;
}

int write(RF24 * rf24, void *buf, uint8_t len) {
    return writeMulticast(rf24, buf, len, 0);
}
/****************************************************************************/

//For general use, the interrupt flags are not important to clear
int writeBlocking(RF24 * rf24, void *buf, uint8_t len, uint32_t timeout) {
    //Block until the FIFO is NOT full.
    //Keep track of the MAX retries and set auto-retry if seeing failures
    //This way the FIFO will fill up and allow blocking until packets go through
    //The radio will auto-clear everything in the FIFO as long as CE remains high

    uint16_t timer = 0;                              //Get the time that the payload transmission started

    while ((get_status(rf24) &
            (_BV(TX_FULL)))) {          //Blocking only if FIFO is full. This will loop and block until TX is successful or timeout

        if (get_status(rf24) & _BV(MAX_RT)) {                      //If MAX Retries have been reached
            reUseTX(rf24);                                          //Set re-transmit and clear the MAX_RT interrupt flag
            if (timer >
                timeout) { return 0; }          //If this payload has exceeded the user-defined timeout, exit and return 0
        }
#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
            if(millis() - timer > (timeout+95) ){
                errNotify();
#if defined (FAILURE_HANDLING)
                return 0;
#endif
            }
#endif
        delay(1);
        timer++;
    }

    //Start Writing
    startFastWrite(rf24, buf, len, 0, 1);                                  //Write the payload if a buffer is clear

    return 1;                                                  //Return 1 to indicate successful transmission
}

/****************************************************************************/

void reUseTX(RF24 * rf24) {
    write_register_byte(rf24, NRF_STATUS, _BV(MAX_RT));              //Clear max retry flag
    spiTrans(rf24, REUSE_TX_PL);
    ce(0);                                          //Re-Transfer packet
    ce(1);
}

/****************************************************************************/

int writeFastMulticast(RF24 * rf24, void *buf, uint8_t len, int multicast) {
    //Block until the FIFO is NOT full.
    //Keep track of the MAX retries and set auto-retry if seeing failures
    //Return 0 so the user can control the retrys and set a timer or failure counter if required
    //The radio will auto-clear everything in the FIFO as long as CE remains high

#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
    uint32_t timer = millis();
#endif

    while ((get_status(rf24) &
            (_BV(TX_FULL)))) {              //Blocking only if FIFO is full. This will loop and block until TX is successful or fail

        if (get_status(rf24) & _BV(MAX_RT)) {
            //reUseTX();										  //Set re-transmit
            write_register_byte(rf24, NRF_STATUS, _BV(MAX_RT));              //Clear max retry flag
            return 0;                                          //Return 0. The previous payload has been retransmitted
            //From the user perspective, if you get a 0, just keep trying to send the same payload
        }
#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
        if(millis() - timer > 95 ){
            errNotify();
#if defined (FAILURE_HANDLING)
            return 0;
#endif
        }
#endif
    }
    //Start Writing
    startFastWrite(rf24, buf, len, multicast, 1);

    return 1;
}

int writeFast(RF24 * rf24, void *buf, uint8_t len) {
    return writeFastMulticast(rf24, buf, len, 0);
}

/****************************************************************************/

//Per the documentation, we want to set PTX Mode when not listening. Then all we do is write data and set CE high
//In this mode, if we can keep the FIFO buffers loaded, packets will transmit immediately (no 130us delay)
//Otherwise we enter Standby-II mode, which is still faster than standby mode
//Also, we remove the need to keep writing the config register over and over and delaying for 150 us each time if sending a stream of data

void startFastWrite(RF24 * rf24, void *buf, uint8_t len, int multicast, int startTx) { //TMRh20

    //write_payload( buf,len);
    write_payload(rf24, buf, len, multicast ? W_TX_PAYLOAD_NO_ACK : W_TX_PAYLOAD);
    if (startTx) {
        ce(1);
    }

}

/****************************************************************************/

//Added the original startWrite back in so users can still use interrupts, ack payloads, etc
//Allows the library to pass all tests
void startWrite(RF24 * rf24, void *buf, uint8_t len, int multicast) {

    // Send the payload

    //write_payload( buf, len );
    write_payload(rf24, buf, len, multicast ? W_TX_PAYLOAD_NO_ACK : W_TX_PAYLOAD);
    ce(1);
#if defined(CORE_TEENSY) || !defined(ARDUINO) || defined (RF24_SPIDEV) || defined (RF24_DUE)
    delayMicroseconds(10);
#endif
    ce(0);


}

/****************************************************************************/

int rxFifoFull(RF24 * rf24) {
    return read_register_byte(rf24, FIFO_STATUS) & _BV(RX_FULL);
}

/****************************************************************************/

int txStandBy(RF24 * rf24) {

#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
    uint32_t timeout = millis();
#endif
    while (!(read_register_byte(rf24, FIFO_STATUS) & _BV(TX_EMPTY))) {
        if (get_status(rf24) & _BV(MAX_RT)) {
            write_register_byte(rf24, NRF_STATUS, _BV(MAX_RT));
            ce(0);
            flush_tx(rf24);    //Non blocking, flush the data
            return 0;
        }
#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
        if( millis() - timeout > 95){
            errNotify();
#if defined (FAILURE_HANDLING)
            return 0;
#endif
        }
#endif
    }

    ce(0);               //Set STANDBY-I mode
    return 1;
}

/****************************************************************************/

int txStandByTimeout(RF24 * rf24, uint32_t timeout, int startTx) {

    if (startTx) {
        stopListening(rf24);
        ce(1);
    }
    uint16_t timer = 0;

    while (!(read_register_byte(rf24, FIFO_STATUS) & _BV(TX_EMPTY))) {
        if (get_status(rf24) & _BV(MAX_RT)) {
            write_register_byte(rf24, NRF_STATUS, _BV(MAX_RT));
            ce(0);                                          //Set re-transmit
            ce(1);
            if (timer >= timeout) {
                ce(0);
                flush_tx(rf24);
                return 0;
            }

        }
        delay(1);
        timer++;
#if defined (FAILURE_HANDLING) || defined (RF24_LINUX)
        if( millis() - start > (timeout+95)){
            errNotify();
#if defined (FAILURE_HANDLING)
            return 0;
#endif
        }
#endif
    }


    ce(0);                   //Set STANDBY-I mode
    return 1;

}

/****************************************************************************/

void maskIRQ(RF24 * rf24, int tx, int fail, int rx) {

    uint8_t config = read_register_byte(rf24, NRF_CONFIG);
    /* clear the interrupt flags */
    config &= ~(1 << MASK_MAX_RT | 1 << MASK_TX_DS | 1 << MASK_RX_DR);
    /* set the specified interrupt flags */
    config |= fail << MASK_MAX_RT | tx << MASK_TX_DS | rx << MASK_RX_DR;
    write_register_byte(rf24, NRF_CONFIG, config);
}

/****************************************************************************/

uint8_t getDynamicPayloadSize(RF24 * rf24) {
    uint8_t result = 0;

#if defined (RF24_LINUX)
    spi_txbuff[0] = R_RX_PL_WID;
    spi_rxbuff[1] = 0xff;
    beginTransaction();
    SPI_MASTER_TRANSCEIVERnb( (char *) spi_txbuff, (char *) spi_rxbuff, 2);
    result = spi_rxbuff[1];
    endTransaction();
#else
    beginTransaction();
    SPI_MASTER_TRANSCEIVER(rf24, R_RX_PL_WID);
    result = SPI_MASTER_TRANSCEIVER(rf24, 0xff);
    endTransaction();
#endif

    if (result > 32) {
        flush_rx(rf24);
        delay(2);
        return 0;
    }
    return result;
}

/****************************************************************************/

int available(RF24 * rf24) {
    return availableFIFO(rf24, NULL);
}

/****************************************************************************/

int availableFIFO(RF24 * rf24, uint8_t *pipe_num) {
    if (!(read_register_byte(rf24, FIFO_STATUS) & _BV(RX_EMPTY))) {

        // If the caller wants the pipe number, include that
        if (pipe_num) {
            uint8_t status = get_status(rf24);
            *pipe_num = (status >> RX_P_NO) & 0x07;
        }
        return 1;
    }


    return 0;


}

/****************************************************************************/

void read(RF24 * rf24, void *buf, uint8_t len) {

    // Fetch the payload
    read_payload(rf24, buf, len);

    //Clear the two possible interrupt flags with one command
    write_register_byte(rf24, NRF_STATUS, _BV(RX_DR) | _BV(MAX_RT) | _BV(TX_DS));

}

/****************************************************************************/

void whatHappened(RF24 * rf24, int *tx_ok, int *tx_fail, int *rx_ready) {
    // Read the status & reset the status in one easy call
    // Or is that such a good idea?
    uint8_t status = write_register_byte(rf24, NRF_STATUS, _BV(RX_DR) | _BV(TX_DS) | _BV(MAX_RT));

    // Report to the user what happened
    *tx_ok = status & _BV(TX_DS);
    *tx_fail = status & _BV(MAX_RT);
    *rx_ready = status & _BV(RX_DR);
}

/****************************************************************************/

void openWritingPipeOld(RF24 * rf24, uint64_t value) {
    // Note that AVR 8-bit uC's store this LSB first, and the NRF24L01(+)
    // expects it LSB first too, so we're good.

    write_register(rf24, RX_ADDR_P0, (uint8_t *)(&value), rf24->addr_width);
    write_register(rf24, TX_ADDR, (uint8_t *)(&value), rf24->addr_width);


    //const uint8_t max_payload_size = 32;
    //write_register_byte(RX_PW_P0,rf24_min(payload_size,max_payload_size));
    write_register_byte(rf24, RX_PW_P0, rf24->payload_size);
}

/****************************************************************************/
void openWritingPipe(RF24 * rf24, uint8_t *address) {
    // Note that AVR 8-bit uC's store this LSB first, and the NRF24L01(+)
    // expects it LSB first too, so we're good.

    write_register(rf24, RX_ADDR_P0, address, rf24->addr_width);
    write_register(rf24, TX_ADDR, address, rf24->addr_width);

    //const uint8_t max_payload_size = 32;
    //write_register_byte(RX_PW_P0,rf24_min(payload_size,max_payload_size));
    write_register_byte(rf24, RX_PW_P0, rf24->payload_size);
}

/****************************************************************************/
static uint8_t child_pipe[] PROGMEM =
        {
                RX_ADDR_P0, RX_ADDR_P1, RX_ADDR_P2, RX_ADDR_P3, RX_ADDR_P4, RX_ADDR_P5
        };
static uint8_t child_payload_size[] PROGMEM =
        {
                RX_PW_P0, RX_PW_P1, RX_PW_P2, RX_PW_P3, RX_PW_P4, RX_PW_P5
        };


void openReadingPipeOld(RF24 * rf24, uint8_t child, uint64_t address) {
    // If this is pipe 0, cache the address.  This is needed because
    // openWritingPipe() will overwrite the pipe 0 address, so
    // startListening() will have to restore it.
    if (child == 0) {
        memcpy(rf24->pipe0_reading_address, &address, rf24->addr_width);
    }

    if (child <= 6) {
        // For pipes 2-5, only write the LSB
        if (child < 2)
            write_register(rf24, pgm_read_byte(&child_pipe[child]), (uint8_t *)(&address), rf24->addr_width);
        else
            write_register(rf24, pgm_read_byte(&child_pipe[child]), (uint8_t *)(&address), 1);

        write_register_byte(rf24, pgm_read_byte(&child_payload_size[child]), rf24->payload_size);

        // Note it would be more efficient to set all of the bits for all open
        // pipes at once.  However, I thought it would make the calling code
        // more simple to do it this way.
        write_register_byte(rf24, EN_RXADDR, read_register_byte(rf24, EN_RXADDR) | _BV(pgm_read_byte(&child_pipe_enable[child])));
    }
}

/****************************************************************************/
void setAddressWidth(RF24 * rf24, uint8_t a_width) {

    if (a_width -= 2) {
        write_register_byte(rf24, SETUP_AW, a_width % 4);
        rf24->addr_width = (a_width % 4) + 2;
    } else {
        write_register_byte(rf24, SETUP_AW, 0);
        rf24->addr_width = 2;
    }

}

/****************************************************************************/

void openReadingPipe(RF24 * rf24, uint8_t child, uint8_t *address) {
    // If this is pipe 0, cache the address.  This is needed because
    // openWritingPipe() will overwrite the pipe 0 address, so
    // startListening() will have to restore it.
    if (child == 0) {
        memcpy(rf24->pipe0_reading_address, address, rf24->addr_width);
    }
    if (child <= 6) {
        // For pipes 2-5, only write the LSB
        if (child < 2) {
            write_register(rf24, pgm_read_byte(&child_pipe[child]), address, rf24->addr_width);
        } else {
            write_register(rf24, pgm_read_byte(&child_pipe[child]), address, 1);
        }
        write_register_byte(rf24, pgm_read_byte(&child_payload_size[child]), rf24->payload_size);

        // Note it would be more efficient to set all of the bits for all open
        // pipes at once.  However, I thought it would make the calling code
        // more simple to do it this way.
        write_register_byte(rf24, EN_RXADDR, read_register_byte(rf24, EN_RXADDR) | _BV(pgm_read_byte(&child_pipe_enable[child])));

    }
}

/****************************************************************************/

void closeReadingPipe(RF24 * rf24, uint8_t pipe) {
    write_register_byte(rf24, EN_RXADDR, read_register_byte(rf24, EN_RXADDR) & ~_BV(pgm_read_byte(&child_pipe_enable[pipe])));
}

/****************************************************************************/

void toggle_features(RF24 * rf24) {
    beginTransaction();
    SPI_MASTER_TRANSCEIVER(rf24, ACTIVATE);
    SPI_MASTER_TRANSCEIVER(rf24, 0x73);
    endTransaction();
}

/****************************************************************************/

void enableDynamicPayloads(RF24 * rf24) {
    // Enable dynamic payload throughout the system

    //toggle_features();
    write_register_byte(rf24, FEATURE, read_register_byte(rf24, FEATURE) | _BV(EN_DPL));

    // Enable dynamic payload on all pipes
    //
    // Not sure the use case of only having dynamic payload on certain
    // pipes, so the library does not support it.
    write_register_byte(rf24, DYNPD, read_register_byte(rf24, DYNPD) | _BV(DPL_P5) | _BV(DPL_P4) | _BV(DPL_P3) | _BV(DPL_P2) | _BV(DPL_P1) |
                          _BV(DPL_P0));

    rf24->dynamic_payloads_enabled = true;
}

/****************************************************************************/
void disableDynamicPayloads(RF24 * rf24) {
    // Disables dynamic payload throughout the system.  Also disables Ack Payloads

    //toggle_features();
    write_register_byte(rf24, FEATURE, 0);

    // Disable dynamic payload on all pipes
    //
    // Not sure the use case of only having dynamic payload on certain
    // pipes, so the library does not support it.
    write_register_byte(rf24, DYNPD, 0);

    rf24->dynamic_payloads_enabled = false;
}

/****************************************************************************/

void enableAckPayload(RF24 * rf24) {
    //
    // enable ack payload and dynamic payload features
    //

    //toggle_features();
    write_register_byte(rf24, FEATURE, read_register_byte(rf24, FEATURE) | _BV(EN_ACK_PAY) | _BV(EN_DPL));

    //
    // Enable dynamic payload on pipes 0 & 1
    //

    write_register_byte(rf24, DYNPD, read_register_byte(rf24, DYNPD) | _BV(DPL_P1) | _BV(DPL_P0));
    rf24->dynamic_payloads_enabled = true;
}

/****************************************************************************/

void enableDynamicAck(RF24 * rf24) {
    //
    // enable dynamic ack features
    //
    //toggle_features();
    write_register_byte(rf24, FEATURE, read_register_byte(rf24, FEATURE) | _BV(EN_DYN_ACK));


}

/****************************************************************************/

void writeAckPayload(RF24 * rf24, uint8_t pipe, void *buf, uint8_t len) {
    uint8_t *current = (uint8_t *)(buf);

    uint8_t data_len = rf24_min(len, 32);

#if defined (RF24_LINUX)
    beginTransaction();
    uint8_t * ptx = spi_txbuff;
    uint8_t size = data_len + 1 ; // Add register value to transmit buffer
    *ptx++ =  W_ACK_PAYLOAD | ( pipe & 0x07 );
    while ( data_len-- ){
      *ptx++ =  *current++;
    }

    SPI_MASTER_TRANSCEIVERn( (char *) spi_txbuff, size);
    endTransaction();
#else
    beginTransaction();
    SPI_MASTER_TRANSCEIVER(rf24, W_ACK_PAYLOAD | (pipe & 0x07));

    while (data_len--)
        SPI_MASTER_TRANSCEIVER(rf24, *current++);
    endTransaction();

#endif

}

/****************************************************************************/

int isAckPayloadAvailable(RF24 * rf24) {
    return !(read_register_byte(rf24, FIFO_STATUS) & _BV(RX_EMPTY));
}

/****************************************************************************/

int isPVariant(RF24 * rf24) {
    return rf24->p_variant;
}

/****************************************************************************/

void setAutoAck(RF24 * rf24, int enable) {
    if (enable)
        write_register_byte(rf24, EN_AA, 0x3F);
    else
        write_register_byte(rf24, EN_AA, 0);
}

/****************************************************************************/

void setAutoAckPipeline(RF24 * rf24, uint8_t pipe, int enable) {
    if (pipe <= 6) {
        uint8_t en_aa = read_register_byte(rf24,EN_AA);
        if (enable) {
            en_aa |= _BV(pipe);
        } else {
            en_aa &= ~_BV(pipe);
        }
        write_register_byte(rf24, EN_AA, en_aa);
    }
}

/****************************************************************************/

int testCarrier(RF24 * rf24) {
    return (read_register_byte(rf24, CD) & 1);
}

/****************************************************************************/

int testRPD(RF24 * rf24) {
    return (read_register_byte(rf24, RPD) & 1);
}

/****************************************************************************/

int isValid(RF24 * rf24) { return 1; }

void setPALevel(RF24 * rf24, uint8_t level) {

    uint8_t setup = read_register_byte(rf24, RF_SETUP) & 0xF8;

    if (level > 3) {                        // If invalid level, go to max PA
        level = (RF24_PA_MAX << 1) + 1;        // +1 to support the SI24R1 chip extra bit
    } else {
        level = (level << 1) + 1;            // Else set level as requested
    }


    write_register_byte(rf24, RF_SETUP, setup |= level);    // Write it to the chip
}

/****************************************************************************/

uint8_t getPALevel(RF24 * rf24) {

    return (read_register_byte(rf24, RF_SETUP) & (_BV(RF_PWR_LOW) | _BV(RF_PWR_HIGH))) >> 1;
}

/****************************************************************************/

int setDataRate(RF24 * rf24, rf24_datarate_e speed) {
    int result = false;
    uint8_t setup = read_register_byte(rf24, RF_SETUP);

    // HIGH and LOW '00' is 1Mbs - our default
    setup &= ~(_BV(RF_DR_LOW) | _BV(RF_DR_HIGH));

    rf24->txDelay = 85;

    if (speed == RF24_250KBPS) {
        // Must set the RF_DR_LOW to 1; RF_DR_HIGH (used to be RF_DR) is already 0
        // Making it '10'.
        setup |= _BV(RF_DR_LOW);
        rf24->txDelay = 155;
    } else {
        // Set 2Mbs, RF_DR (RF_DR_HIGH) is set 1
        // Making it '01'
        if (speed == RF24_2MBPS) {
            setup |= _BV(RF_DR_HIGH);
            rf24->txDelay = 65;
        }
    }
    write_register_byte(rf24, RF_SETUP, setup);

    // Verify our result
    if (read_register_byte(rf24, RF_SETUP) == setup) {
        result = true;
    }
    return result;
}

/****************************************************************************/

rf24_datarate_e getDataRate(RF24 * rf24) {
    rf24_datarate_e result;
    uint8_t dr = read_register_byte(rf24, RF_SETUP) & (_BV(RF_DR_LOW) | _BV(RF_DR_HIGH));

    // switch uses RAM (evil!)
    // Order matters in our case below
    if (dr == _BV(RF_DR_LOW)) {
        // '10' = 250KBPS
        result = RF24_250KBPS;
    } else if (dr == _BV(RF_DR_HIGH)) {
        // '01' = 2MBPS
        result = RF24_2MBPS;
    } else {
        // '00' = 1MBPS
        result = RF24_1MBPS;
    }
    return result;
}

/****************************************************************************/

void setCRCLength(RF24 * rf24, rf24_crclength_e length) {
    uint8_t config = read_register_byte(rf24, NRF_CONFIG) & ~(_BV(CRCO) | _BV(EN_CRC));

    // switch uses RAM (evil!)
    if (length == RF24_CRC_DISABLED) {
        // Do nothing, we turned it off above.
    } else if (length == RF24_CRC_8) {
        config |= _BV(EN_CRC);
    } else {
        config |= _BV(EN_CRC);
        config |= _BV(CRCO);
    }
    write_register_byte(rf24, NRF_CONFIG, config);
}

/****************************************************************************/

rf24_crclength_e getCRCLength(RF24 * rf24) {
    rf24_crclength_e result = RF24_CRC_DISABLED;

    uint8_t config = read_register_byte(rf24, NRF_CONFIG) & (_BV(CRCO) | _BV(EN_CRC));
    uint8_t AA = read_register_byte(rf24, EN_AA);

    if (config & _BV(EN_CRC) || AA) {
        if (config & _BV(CRCO))
            result = RF24_CRC_16;
        else
            result = RF24_CRC_8;
    }

    return result;
}

/****************************************************************************/

void disableCRC(RF24 * rf24) {
    uint8_t disable = read_register_byte(rf24, NRF_CONFIG) & ~_BV(EN_CRC);
    write_register_byte(rf24, NRF_CONFIG, disable);
}

/****************************************************************************/
void setRetries(RF24 * rf24, uint8_t delay, uint8_t count) {
    write_register_byte(rf24, SETUP_RETR, (delay & 0xf) << ARD | (count & 0xf) << ARC);
}


//ATTiny support code pulled in from https://github.com/jscrane/RF24

#if defined(__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
// see http://gammon.com.au/spi
#	define DI   0  // D0, pin 5  Data In
#	define DO   1  // D1, pin 6  Data Out (this is *not* MOSI)
#	define USCK 2  // D2, pin 7  Universal Serial Interface clock
#	define SS   3  // D3, pin 2  Slave Select
#elif defined(__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
// these depend on the core used (check pins_arduino.h)
// this is for jeelabs' one (based on google-code core)
#	define DI   4   // PA6
#	define DO   5   // PA5
#	define USCK 6   // PA4
#	define SS   3   // PA7
#elif defined(__AVR_ATtiny2313__) || defined(__AVR_ATtiny4313__)
// these depend on the core used (check pins_arduino.h)
// tested with google-code core
#	define DI   14  // PB5
#	define DO   15  // PB6
#	define USCK 16  // PB7
#	define SS   13  // PB4
#elif defined(__AVR_ATtiny861__)
// these depend on the core used (check pins_arduino.h)
// tested with google-code core
#    define DI   9   // PB0
#    define DO   8   // PB1
#    define USCK 7   // PB2
#    define SS   6   // PB3
#endif

#if defined(RF24_TINY)

void SPIClass::begin() {

  pinMode(USCK, OUTPUT);
  pinMode(DO, OUTPUT);
  pinMode(DI, INPUT);
  USICR = _BV(USIWM0);

}

byte SPIClass::transfer(byte b) {

  USIDR = b;
  USISR = _BV(USIOIF);
  do
    USICR = _BV(USIWM0) | _BV(USICS1) | _BV(USICLK) | _BV(USITC);
  while ((USISR & _BV(USIOIF)) == 0);
  return USIDR;

}

void SPIClass::end() {}
void SPIClass::setDataMode(uint8_t mode){}
void SPIClass::setBitOrder(uint8_t bitOrder){}
void SPIClass::setClockDivider(uint8_t rate){}


#endif
