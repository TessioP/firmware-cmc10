/*
 * PIDController.c
 *
 *  Created on: Sep 20, 2018
 *      Author: mmaximo
 */

#include "PIDController.h"

void initializePIDController(PIDController *controller, float Kp, float Ki, float Kd, float samplingFrequency, float maxCommand) {
	controller->Kp = Kp;
	controller->Ki = Ki;
	controller->Kd = Kd;
	controller->errorIntegral = 0.0;
	controller->previousError = 0.0;
	controller->sampleTime = 1.0 / samplingFrequency;
	controller->maxCommand = maxCommand;
}

float pidControl(PIDController *controller, float reference, float speed) {
	static float error, errorDerivative, value;
	error = reference - speed;
	errorDerivative = (error - controller->previousError) / controller->sampleTime;
	value = controller->Kp * error + controller->Ki * controller->errorIntegral + controller->Kd * errorDerivative;
	controller->errorIntegral += error * controller->sampleTime;
	if (value > controller->maxCommand) {
		value = controller->maxCommand;
		controller->errorIntegral = controller->maxCommand / controller->Ki;
	} else if (value < -controller->maxCommand) {
		value = -controller->maxCommand;
		controller->errorIntegral = -controller->maxCommand / controller->Ki;
	}
	controller->previousError = error;
	return value;
}

void setPIDGains(PIDController *controller, float Kp, float Ki, float Kd) {
	controller->Kp = Kp;
	controller->Ki = Ki;
	controller->Kd = Kd;
}
