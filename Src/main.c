/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2018 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include <PIDController.h>
#include "main.h"
#include "stm32f3xx_hal.h"
#include "adc.h"
#include "spi.h"
#include "tim.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include "RF24.h"
#include "RF24_config.h"
#include "definitions.h"
#include "Motor.h"
#include "protocol.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_NVIC_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */
float batteryVoltage;
float getBatteryVoltage();

char receiveData[MSG_SIZE];
RF24 radio;

PIDController leftController, rightController;
Motor leftMotor, rightMotor;
float speedHistory[100];
int controlCounter = 0;

// Create global variables to handle the motor references
volatile float referenceRight = -2000.0, referenceLeft = -2000.0; // in rad/s
int16_t referenceRightRpm = 0, referenceLeftRpm = 0;            // in rpm

dataHandler messageParser;

void isrRadio() { // ISR: Interrupt Service Routine
	read(&radio, receiveData, MSG_SIZE);
    decode(&messageParser, receiveData);
    if (messageParser.hasToWriteMotorSpeed) {
		referenceLeftRpm = messageParser.leftMotorSpeed;
		referenceRightRpm = messageParser.rightMotorSpeed;
		referenceLeft = referenceLeftRpm * RPM_TO_SI;
		referenceRight = referenceRightRpm * RPM_TO_SI;
    } else if (messageParser.hasToUpdateGainsPID) {
    	setPIDGains(&leftController, messageParser.Kp, messageParser.Ki, messageParser.Kd);
    	setPIDGains(&rightController, messageParser.Kp, messageParser.Ki, messageParser.Kd);
    }
}

void isr1000Hz() {

}

void isr200Hz() {
	updateMotor(&leftMotor);
	updateMotor(&rightMotor);
	float speedLeft = 0.0, speedRight = 0.0;
	float commandLeft = 0.0, commandRight = 0.0;
	int pwmLeft, pwmRight;
	if (batteryVoltage > MINIMUM_BATTERY_VOLTAGE) {
		speedLeft = getCurrentMotorSpeed(&leftMotor);
		speedRight = getCurrentMotorSpeed(&rightMotor);
		commandLeft = pidControl(&leftController, referenceLeft, speedLeft);
		commandRight = pidControl(&rightController, referenceRight, speedRight);
		pwmLeft = leftMotor.maxPwm * commandLeft / NOMINAL_BATTERY_VOLTAGE;
		pwmRight = rightMotor.maxPwm * commandRight / NOMINAL_BATTERY_VOLTAGE;
	} else {
		pwmLeft = 0;
		pwmRight = 0;
	}
	setDriverCommand(&leftMotor, pwmLeft);
	setDriverCommand(&rightMotor, pwmRight);
	speedHistory[controlCounter] = speedLeft;
	++controlCounter;
	if (controlCounter == 100) {
		controlCounter = 0;
	}
}

void isr100Hz() {

}

void isr1Hz() {
	batteryVoltage = getBatteryVoltage();
}

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void) {
	/* USER CODE BEGIN 1 */

	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_SPI1_Init();
	MX_TIM1_Init();
	MX_TIM2_Init();
	MX_TIM3_Init();
	MX_ADC1_Init();
	MX_TIM17_Init();

	/* Initialize interrupts */
	MX_NVIC_Init();
	/* USER CODE BEGIN 2 */

	messageParser.robotID = 1;

	batteryVoltage = getBatteryVoltage();

	initializeMotor(&leftMotor, &htim1, TIM_CHANNEL_1, TIM_CHANNEL_4, &htim3, MOTOR_LEFT, 200.0);
	initializeMotor(&rightMotor, &htim1, TIM_CHANNEL_2, TIM_CHANNEL_3, &htim2, MOTOR_RIGHT, 200.0);
	initializePIDController(&leftController, 0.0043, 0.1981, 0.0, 200.0, NOMINAL_BATTERY_VOLTAGE);
	initializePIDController(&rightController, 0.0043, 0.1981, 0.0, 200.0, NOMINAL_BATTERY_VOLTAGE);

	HAL_TIM_Base_Init(&htim17);
	HAL_TIM_Base_Start_IT(&htim17);

	// Radio nRF24L01
	initializeRF24(&radio, CE_GPIO_Port, CE_Pin, CSN_RADIO_GPIO_Port,
	CSN_RADIO_Pin, &hspi1);
	begin(&radio);
	setPayloadSize(&radio, MSG_SIZE);
	setChannel(&radio, 100);
	setDataRate(&radio, RF24_2MBPS);
	maskIRQ(&radio, 1, 1, 0);         // Allows only RX events
	setCRCLength(&radio, RF24_CRC_8);
	setPALevel(&radio, RF24_PA_MIN);
	//radio.disableCRC();
	setAutoAck(&radio, 0);
	HAL_Delay(100);

	// Open communication pipes
	openReadingPipe(&radio, 0, broadcastAddress);
	startListening(&radio);

	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1) {
		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */
	}
	/* USER CODE END 3 */

}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;
	RCC_PeriphCLKInitTypeDef PeriphClkInit;

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_TIM1;
	PeriphClkInit.Tim1ClockSelection = RCC_TIM1CLK_HCLK;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/**
 * @brief NVIC Configuration.
 * @retval None
 */
static void MX_NVIC_Init(void) {
	/* TIM1_TRG_COM_TIM17_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(TIM1_TRG_COM_TIM17_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(TIM1_TRG_COM_TIM17_IRQn);
	/* EXTI2_TSC_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(EXTI2_TSC_IRQn, 0, 0);
	HAL_NVIC_EnableIRQ(EXTI2_TSC_IRQn);
}

/* USER CODE BEGIN 4 */
float getBatteryVoltage() {
	HAL_ADC_Start(&hadc1);
	HAL_ADC_PollForConversion(&hadc1, 1);
	uint32_t batteryVoltageRaw = HAL_ADC_GetValue(&hadc1);
	return batteryVoltageRaw * 3.3 / 4095.0 * (22.2 + 2.2) / 2.2;
}
/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
