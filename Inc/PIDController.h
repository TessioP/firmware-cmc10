/*
 * PIDController.h
 *
 *  Created on: Sep 20, 2018
 *      Author: mmaximo
 */

#ifndef PIDCONTROLLER_H_
#define PIDCONTROLLER_H_

struct PIDController {
	volatile float Kp;
	volatile float Ki;
	volatile float Kd;
	float previousError;
	float errorIntegral;
	float sampleTime;
	float maxCommand;
};

typedef struct PIDController PIDController;

void initializePIDController(PIDController *controller, float Kp, float Ki, float Kd, float samplingFrequency, float maxCommand);

float pidControl(PIDController *controller, float reference, float speed);

void setPIDGains(PIDController *controller, float Kp, float Ki, float Kd);

#endif /* PIDCONTROLLER_H_ */
