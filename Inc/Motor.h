/*
 * Motor.h
 *
 *  Created on: Sep 19, 2018
 *      Author: mmaximo
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "tim.h"
#include "Encoder.h"

enum MOTOR_TYPE {
	MOTOR_LEFT, MOTOR_RIGHT
};

struct Motor {
	/**
	 * Control parameters
	 */
	float pwm;
	float samplingFrequency;

	/*
	 * Motor Type
	 */
	int type;

	/*
	 * Sensor
	 */
	Encoder encoder;

	/*
	 * Hardware parameters
	 */
	TIM_HandleTypeDef* pwmTimer;
	int channelA;
	int channelB;
	int maxPwm;
};

typedef struct Motor Motor;

void initializeMotor(Motor *motor, TIM_HandleTypeDef *pwmTimer, int channelA,
		int channelB, TIM_HandleTypeDef *encoderTime, int type,
		float samplingFrequency);

void setDriverCommand(Motor *motor, int value);

void updateMotor(Motor *motor);

float getCurrentMotorSpeed(Motor *motor);

#endif /* MOTOR_H_ */
