/*
 * protocol.h
 *
 *  Created on: Aug 8, 2018
 *      Author: tessi
 */

#ifndef PROTOCOL_H_
#define PROTOCOL_H_

#include <stdio.h>
#include <string.h>
#include <definitions.h>

struct dataHandler {
    // PID gains and service order
    float Kp, Ki, Kd;

    // Motors reference speed
    int16_t leftMotorSpeed;
    int16_t rightMotorSpeed;

    //robotID
    uint8_t robotID;

    // Data integrity
    int checksumOK;

    // Need to update PID gains?
    int hasToUpdateGainsPID;

    // Must report battery voltage?
    int hasToReadBatteryVoltage;

    // Must report motors velocity?
    int hasToReadMotorSpeed;

    // Set reference speed?
    int hasToWriteMotorSpeed;

    // Need to send an answer to master?
    int needToRespond;
};

typedef struct dataHandler dataHandler;


// Constructor
void dataHandlerInit(dataHandler* dataHandler, uint8_t robotID);

// Functions to handle the numerical conversion
float hex2float(char* packet);
void float2hex(float number, char* data);
int16_t hex2int16(char* packet);

// Data integrity
int verifyCkecksum(char* packet);

// Encode and decode messages
void encode(dataHandler* dataHandler, char *packet, float rightMotorSpeed, float leftMotorSpeed, float batteryVoltage);
void decode(dataHandler* dataHandler, char *packet);

// Get functions
void getMotorSpeed(dataHandler* dataHandler, int16_t* leftMotorVelocity, int16_t* rightMotorVelocity);

#endif /* PROTOCOL_H_ */
