/*
 * definitions.h
 *
 *  Created on: 5 de nov de 2017
 *      Author: junior
 */
#include "stm32f3xx_hal.h"
#include "tim.h"

#ifndef DEFINITIONS_H_
#define DEFINITIONS_H_

/// Sample period
//constexpr float Ts = 20e-3;
//constexpr int miliTs = 20;

/// H-bridge parameters
//constexpr float h_bridge_Vloss = 1.3;

/// Motor parameters
//constexpr float Kgear = 51.45;
//constexpr float umin  = -6.0;
//constexpr float umax  =  6.0;

/// Encoder parameters
//constexpr int   CPR        = 12;
//constexpr float Kenc       = 1./(Ts * Kgear * CPR);

/// PID tuning
//constexpr float Kp = 5*0.027;
//constexpr float Ki = 5;
//constexpr float Kd = 0.0;

/// Radio
extern uint8_t masterAddress[6];
extern uint8_t broadcastAddress[6];

/*Physical parameters*/
#define PI 3.141592
#define SOURCE_CLOCK 72000000.0
#define WHEEL_RADIUS 0.0259
#define GRAVITY 9.80665
#define STATIC_FRICTION_COEF 0.7
#define REDUCTION (119.0 / 12.0)
#define MAX_ANGULAR_ACCELERATION GRAVITY * STATIC_FRICTION_COEF / WHEEL_RADIUS
#define PULSES_PER_TURN 12.0

#define NOMINAL_BATTERY_VOLTAGE 7.9
#define MINIMUM_BATTERY_VOLTAGE 6.4


/*Conversion factors*/

#define RPM_TO_SI (2.0 * PI / (60.0))
#define SI_TO_RPM (1.0 / RPM_TO_SI)

// Protocol
#define MSG_SIZE    14
#define WPID        3  // Write PID
#define RBV         2  // Read Battery Voltage
#define RMS         1  // Read Motor Speeds
#define WMS         0  // Write Motor Speeds

/*Control parameters*/
#define CONTROL_LOOP_FREQUENCY



#endif /* DEFINITIONS_H_ */
