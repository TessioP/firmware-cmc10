/*
 * Encoder.h
 *
 *  Created on: Sep 19, 2018
 *      Author: mmaximo
 */

#ifndef ENCODER_H_
#define ENCODER_H_

#include "stm32f3xx_hal.h"
#include "tim.h"

enum ENCODER_TYPE {
	ENCODER_LEFT, ENCODER_RIGHT
};

struct Encoder {
	int16_t currentEncoderCount;
	int16_t previousEncoderCount;
	float samplingFrequency;
	TIM_HandleTypeDef *encoderCounter;
	float pulsesToRpm;
	float pulsesToRad;
};

typedef struct Encoder Encoder;

// construct encoder with encoder counter and sampling frequency
void initializeEncoder(Encoder * encoder, TIM_HandleTypeDef * encoderTimer,
		float samplingFrequency);

// updates encoder pulses values
void updateEncoder(Encoder * encoder);

// returns change in number of pulses
int16_t getEncoderDiff(Encoder * encoder);

// returns speed in rad/s
float getCurrentEncoderSpeed(Encoder * encoder);

float getCurrentEncoderSpeedRPM(Encoder * encoder);

#endif /* ENCODER_H_ */
